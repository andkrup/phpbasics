<?php
namespace supervillainhq\core\contacts{

	use supervillainhq\core\intl\Country;
	use supervillainhq\core\Objectible;

	class PhoneNumber implements \Serializable{
		use Objectible;

		protected $number;
		protected $country;
		protected $region;

		function number($number = null){
			if(!is_null($number)){
				$this->number = $number;
			}
			return $this->number;
		}

		function country(Country $country = null){
			if(!is_null($country)){
				$this->country = $country;
			}
			return $this->country;
		}
		function countryCode(){
			if(!is_null($this->country)){
				return $this->country->iso();
			}
		}
		function region($region = null){
			$this->region = $this->country;
			if(!is_null($region)){
				$this->region = $region;
			}
			return $this->region;
		}
		function regionCode(){
		}

		function serialize(){
			$object = $this->toObject();
			return serialize($object);
		}

		function unserialize ($serialized) {
			// handle the situations where an already unserialized object is passed by recursed unserialize calls
			if(is_null($serialized)){
				return;
			}
			elseif(is_object($serialized)){
				$this->number($serialized->number);
			}
			else{
				// $serialized is a string-interpretation and should be unserialized
				$data = unserialize($serialized); // $serialized is now a stdClass/simple object
				$this->number($data->number);
			}
		}

		static function hydrate($serialized){
			return unserialize($serialized);
		}

		function __toString(){
			return "{$this->number}";
		}
	}
}
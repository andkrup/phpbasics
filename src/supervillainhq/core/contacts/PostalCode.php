<?php
namespace supervillainhq\core\contacts{
	use supervillainhq\core\Objectible;

	class PostalCode implements \Serializable{
		use Objectible;

		private $code;

		function code($code = null){
			if(is_null($code)){
				return $this->code;
			}
			$this->code = $code;
		}


		function __construct($code){
			$this->code = $code;
		}

		function serialize(){
			$object = $this->toObject();
			return serialize($object);
		}
		function unserialize($serialized){
			// handle the situations where an already unserialized object is passed by recursed unserialize calls
			if(is_null($serialized)){
				return;
			}
			elseif(is_object($serialized)){
				$this->code($serialized->code);
			}
			else{
				// $serialized is a string-interpretation and should be unserialized
				if(!($serialized instanceof PostalCode)){
					$serialized = unserialize($serialized); // $serialized is now a stdClass/simple object
				}
				$this->code($serialized->code);
			}
		}

		function __toString(){
			return "{$this->code}";
		}
	}
}
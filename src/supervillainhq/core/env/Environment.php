<?php
namespace supervillainhq\core\env{
	class Environment{
		const DEV = 'dev';
		const TEST = 'test';
		const STAGING = 'staging';
		const PRODUCTION = 'production';
		static $key_env = 'ENV';
		static private $instance;

		protected $config;

		private function __construct($config = null){
			if(is_null($config)){
				$config = [];
			}
			$this->config = $config;
		}

		static function load($config = null){
			if(!isset(self::$instance)){
				self::$instance = new Environment($config);
			}
			return self::$instance;
		}

		public function getEnvValue($key){
			if(!array_key_exists($key, $_SERVER)){
				return false;
			}
			return trim($_SERVER[$key]);
		}

		public function isDev(){
			return false!==stripos($this->getEnvValue(self::$key_env), self::DEV);
		}
		public function isTest(){
			return false!==stripos($this->getEnvValue(self::$key_env), self::TEST);
		}
		public function isStaging(){
			return false!==stripos($this->getEnvValue(self::$key_env), self::STAGING);
		}
		public function isProduction(){
			return false!==stripos($this->getEnvValue(self::$key_env), self::PRODUCTION);
		}
	}
}
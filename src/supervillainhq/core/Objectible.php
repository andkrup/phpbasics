<?php
namespace supervillainhq\core{
	trait Objectible{
		function toArray(){
			return get_object_vars($this);
		}

		function toObject(){
			return (object) get_object_vars($this);
		}
	}
}
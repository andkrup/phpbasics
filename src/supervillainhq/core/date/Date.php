<?php
namespace supervillainhq\core\date{

	class Date extends \DateTime{

		static private $config;
		static protected $defaultFormat = 'Y-m-d H:i:s';


		static function config(array $config = null){
			if(is_null($config)){
				return self::$config;
			}
			self::$config = $config;
		}

		static function getDefaultTimeZone(){
			$config = (object) self::$config;
			return new \DateTimeZone($config->default_timezone);
		}
		static function getDefaultFormat(){
			$config = (object) self::$config;
			return $config->dateformat;
		}
		static function getInputfieldFormat(){
			return 'Y/m/d H:i:s';
		}
		static function getDatabaseFormat(){
			return 'Y-m-d H:i:s';
		}

		function __construct($time = "now", \DateTimeZone $timezone = null){
			if(is_null($timezone)){
				$timezone = self::getDefaultTimeZone();
			}
			parent::__construct($time, $timezone);
		}

		function __toString(){
			$format = self::getDefaultFormat();
			return $this->format($format);
		}

		static function createFromFormat($format, $time, $object = null){
			if(is_null($object)){
				$object = self::getDefaultTimeZone();
			}
			$datetime = \DateTime::createFromFormat($format, $time, $object);
			$instance = new Date($datetime->format(\DateTime::ISO8601), $object);
			$instance->setTimezone($object);

			return $instance;
		}

		function toObject(){
			$object = (object) [
					'time' => $this->format(\DateTime::ISO8601),
					'timezone' => $this->getTimezone()->getName(),
			];
			return $object;
		}
		static function fromObject($object){
			if($object instanceof Date){
				return $object;
			}
			$time = $object->time;
			$timezone = new \DateTimeZone($object->timezone);
			return self::createFromFormat(\DateTime::ISO8601, $time, $timezone);
		}
	}
}
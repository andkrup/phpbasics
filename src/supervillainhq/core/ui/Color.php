<?php
namespace supervillainhq\core\ui{
	class Color{
		private $name;

		function name($name = null){
			if(is_null($name)){
				return $this->name;
			}
			$this->name = $name;
		}

		function __construct($name = null){
			$this->name($name);
		}

		function __toString(){
			return "{$this->name}";
		}
	}
}
<?php
namespace supervillainhq\core{
	interface Command{
		function execute();
	}
}
<?php
namespace supervillainhq\core\locale{
	final class CurrencyFormatter{

		static function percentage($decimal = .25){
			$percentage = $decimal * 100;
			return "{$percentage}%";
		}

		static function money($value){
			$money = $value / 100;
			if(false===strpos("{$money}", '.')){
				return "kr. {$money},-";
			}
			list($dollars, $cents) = explode('.', "{$money}");
			return "kr. {$dollars},{$cents}";
		}
	}
}
<?php
namespace supervillainhq\core\locale{
	trait Localizing{
		private $locale;

		function locale(\Locale $locale = null){
			if(is_null($locale)){
				return $this->locale;
			}
			$this->locale = $locale;
		}

		function language(){
			return \Locale::getPrimaryLanguage($this->locale);
		}

		function region(){
			return \Locale::getRegion($this->locale);
		}
	}
}
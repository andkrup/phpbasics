<?php
namespace supervillainhq\core\locale{
	interface Localized{
		function locale(\Locale $locale = null);
		function language();
		function region();
	}
}
<?php
namespace supervillainhq\core\intl{

	class Country implements \Serializable{

		protected $name;
		protected $isoCodeAlpha2;
		protected $isoCodeAlpha3;
		protected $locales;

		function name($name = null){
			if(is_null($name)){
				return $this->name;
			}
			$this->name = $name;
		}
		function localizedName($locale = null){
//			$this->name = $name;
		}

		/**
		 * 2-letter charactrer code as specified by ISO 3166-1 (alpha-2)
		 *
		 * @param string $isoCode
		 * @return string
		 */
		function isoCodeAlpha2( $isoCode = null){
			if(is_null($isoCode)){
				return $this->isoCodeAlpha2;
			}
			$this->isoCodeAlpha2 = $isoCode;
		}
		/**
		 * 3-letter charactrer code as specified by ISO 3166-1 (alpha-3)
		 *
		 * @param string $isoCode
		 * @return string
		 */
		function isoCodeAlpha3( $isoCode = null){
			if(is_null($isoCode)){
				return $this->isoCodeAlpha3;
			}
			$this->isoCodeAlpha3 = $isoCode;
		}


		protected function __construct(){
			$this->resetLocales();
		}

		static function get($countryName){
			$instance = new Country();
			$instance->name($countryName);
			return $instance;
		}


		function resetLocales(array $locales = []){
			$this->locales = $locales;
		}
		function addLocale($key, $locale){
			$this->locales[$key] = $locale;
		}
		function removeLocale($locale){
			$c = count($this->locales);
			for($i = 0; $i < $c; $i++){
				if($this->locales[$i] == $locale){
					array_splice($this->locales, $i, 0);
				}
			}
		}
		function removeLocaleAt($key){
			unset($this->locales[$key]);
		}
		function hasLocale($locale){
			return in_array($this->locales, $locale);
		}
		function hasLocaleAtKey($key){
			return array_key_exists($key, $this->locales);
		}
		function getLocale($key){
			return $this->locales[$key];
		}
		function locales(){
			return $this->locales;
		}


		function equals(Country $country){
			return $this->name() == $country->name();
		}

		function serialize(){
			$object = (object) ['name'=>$this->name];
			return serialize($object);
		}
		function unserialize($serialized){}


		function __toString(){
			return "{$this->name}";
		}
	}
}
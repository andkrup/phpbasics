<?php
namespace supervillainhq\core\mail{
	class Email{
		private $valid;
		private $confirmed;
		protected $emailAddress;


		function emailAddress($emailAddress = null, $validate = false){
			if(is_null($emailAddress)){
				return $this->emailAddress;
			}
			if($validate){
				$this->validate($emailAddress);
			}
			$this->emailAddress = $emailAddress;
		}
		function valid($recheck = false){
			if($recheck){
				$this->validate($this->emailAddress);
			}
			return $this->valid;
		}

		function __construct($emailAddress, $validate = false){
			if($validate){
				$this->validate($emailAddress);
			}
			$this->emailAddress = $emailAddress;
		}

		private function validate($emailAddress){
			$this->valid = false !== filter_var($emailAddress, FILTER_VALIDATE_EMAIL);
			if(!$this->valid){
				throw new \Exception('email did not validate');
			}
		}

		function __toString(){
			return "{$this->emailAddress}";
		}
	}
}
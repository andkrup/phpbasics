<?php
namespace supervillainhq\core\db{
	trait ParameterContainer{
		protected $parameters;

		function resetParameters(array $parameters = []){
			$this->parameters = $parameters;
		}
		function addParameter($key, $parameter){
			$this->parameters[$key] = $parameter;
		}
		function removeParameter($parameter){
			$c = count($this->parameters);
			for($i = 0; $i < $c; $i++){
				if($this->parameters[$i] == $parameter){
					array_splice($this->parameters, $i, 0);
				}
			}
		}
		function removeParameterAt($key){
			unset($this->parameters[$key]);
		}
		function hasParameter($parameter){
			return in_array($this->parameters, $parameter);
		}
		function hasParameterAtKey($key){
			return array_key_exists($key, $this->parameters);
		}
		function getParameter($key){
			return $this->parameters[$key];
		}
		function parameters(){
			return $this->parameters;
		}

		function emptyParameters($prefix = null){
			$list = $this->parameters();
			// only check a subset of the list
			if(!is_null($prefix)){
				$tmp = [];
				foreach ($list as $key => $value) {
					if (strpos($key, $prefix) === 0) {
						$tmp[$key] = $value;
					}
				}
				$list = $tmp;
			}
			// an empty list is a given true
			if(count($list) === 0){
				return true;
			}
			$values = array_unique(array_values($list)); // don't need to check an array with lot's of nulls
			foreach ($values as $value) {
				// if one element is assigned with a useful value, then it's a given false
				if (!is_null($value) && !empty($value) && '' != $value) {
					return false;
				}
			}
			return true;
		}
	}
}
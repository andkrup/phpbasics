<?php
namespace supervillainhq\core\db{

	/**
	 * The DataWriter is handling the task of mapping domain-level object information into the database.
	 * Use the DataMapper to handle tasks that goes the other way, ie. moving data from the database
	 * into domain-level objects.
	 *
	 * @author ak
	 *
	 */
	interface DataWriter{
		function data($data = null);

		/**
		 * Standard crud below (actually it's only the CUD in CrUD that is defined here)
		 */
		function create();
		function update();
		function delete();
		function __get($name);
	}
}
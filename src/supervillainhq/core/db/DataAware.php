<?php
namespace supervillainhq\core\db{
	trait DataAware{
		protected $id;

		function id($id = null){
			if(is_null($id)){
				return $this->id;
			}
			$this->id = intval($id);
		}
	}
}
<?php
namespace supervillainhq\core\db{

	/**
	 * This is the default implementation of the IsReadable interface
	 * @author ak
	 *
	 */
	trait Reading{
		protected $data;

		function data($data = null){
			if(!is_null($data)){
				$this->data = $data;
			}
			return $this->data;
		}
	}
}
<?php
namespace supervillainhq\core{
	/**
	 * Error message handling implementations
	 *
	 * @author ak
	 */
	trait Erroneous{
		private $errors;

		function resetErrors(array $errors = []){
			$this->errors = $errors;
		}
		function addError( $error){
			array_push($this->errors, $error);
		}
		function removeError( $error){
			$c = count($this->errors);
			for($i = 0; $i < $c; $i++){
				if($this->errors[$i] == $error){
					array_splice($this->errors, $i, 0);
				}
			}
		}
		function hasError($error){
			return in_array($this->errors, $error);
		}
		function getError($index){
			return $this->errors[$index];
		}
		function errors(){
			return $this->errors;
		}
	}
}
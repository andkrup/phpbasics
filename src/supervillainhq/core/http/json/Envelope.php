<?php
/**
 * Created by PhpStorm.
 * User: anders
 * Date: 26/08/16
 * Time: 00:11
 */

namespace supervillainhq\core\http\json{
	interface Envelope extends \JsonSerializable{
		function payload($payload = null);
	}
}


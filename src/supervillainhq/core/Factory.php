<?php
namespace supervillainhq\core{
	interface Factory{
		function create();
	}
}
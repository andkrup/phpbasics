<?php
/**
 * Created by PhpStorm.
 */

namespace supervillainhq\core\io{
	class FileEditor {
		const IMAGE = 1;
		const VIDEO = 2;
		const TEXT = 3;
		const EXECUTABLE = 4;

		protected $file;
		protected $contents;

		function file(\SplFileInfo $file = null){
			if(!is_null($file)){
				$this->file = $file;
			}
			return $this->file;
		}
		function contents( $data){
			if(!is_null($data)){
				$this->contents = $data;
			}
			return $this->contents;
		}

		function __construct(\SplFileInfo $file = null) {
			$this->file($file);
		}

		/**
		 * Creates the path if possible
		 *
		 * @param bool $filepath (default true) If true, then only the path-part is used and the filename is removed when creating the path
		 * @return string Returns the path created
		 * @throws \Exception
		 */
		function mkdir($filepath = true) {
			$dirname = $filepath ? $this->file()->getPath() : $this->file()->getPathname();
			if (!is_dir($dirname)) {
				if(!mkdir($dirname, 0755, true)){
					throw new \Exception("directory '{$dirname}' doesn't exist and can't be created");
				}
			}
			return $dirname;
		}

		/**
		 * Creates the file and writes the contents to it. If the path doesn't exist, the path will be mkdir'ed
		 * @return bool
		 * @throws \Exception
		 */
		function write() {
			$dirname = $this->mkdir();
			if(is_writable($dirname)) {
				if(false !== file_put_contents($this->file()->getPathname(), $this->contents)){
					return true;
				};
			}
			throw new \Exception("can't write file '{$this->file()->getPathname()}'");
		}

		static function create($path){
			$instance = new FileEditor(new \SplFileInfo($path));
//			$instance->write();
			return $instance;
		}

		function read(){
			if($this->file()->isReadable()){
				$this->contents = file_get_contents($this->file()->getRealPath());
			}
		}

		function delete(){}


		public static function deleteDirectory($dir) {
			if (!file_exists($dir)) return true;
			if (!is_dir($dir) || is_link($dir)) return unlink($dir);
			foreach (scandir($dir) as $item) {
				if ($item == '.' || $item == '..') continue;
				if (!FileUtil::deleteDirectory($dir . "/" . $item)) {
					chmod($dir . "/" . $item, 0777);
					if (!FileUtil::deleteDirectory($dir . "/" . $item)) return false;
				};
			}
			return rmdir($dir);
		}

		public static function getFileExtension($filename){
			return pathinfo($filename, PATHINFO_EXTENSION);
		}

		public static function filenameContainsNullChar($filename){
			$charlist = count_chars($filename, 1);
			print_r($charlist);
			return intval($charlist[0]) > 0;
		}

		public static function fileMimetype($filename){
			$finfo = new finfo(FILEINFO_MIME);
			$val = $finfo->file($filename);
			return $val;
		}

		public static function fileIsFiletype($filename, $filetype){
			$finfo = new finfo();
			$val = $finfo->file($filename);
			switch ($filetype){
				case FileUtil::IMAGE:
					return false!==stripos($val, 'image data');
			}
			return false;
		}
		/**
		 * TODO: ward off unwanted malicious files
		 *
		 * @param (multi) $uploadedFile A single file item from the $_FILES array
		 * @return boolean
		 */
		public static function validateUpload($uploadedFile, $validateType = null){
			if(is_array($uploadedFile)){
				$uploadedFile = (object) $uploadedFile;
			}
			// avoid hidden files, such as .htaccess which can be used to change image mime-type to let apache execute file as script
			if(stripos($uploadedFile->name, '.')==0){
				return -666;
			}
			if(!FileUtil::fileIsFiletype($uploadedFile->tmp_name, $validateType)){
				return -667;
			}
			return 0;
		}

		public static function fileCount($directory){
			$fi = new FilesystemIterator($directory, FilesystemIterator::SKIP_DOTS);
			return intval(iterator_count($fi));
		}
	}
}



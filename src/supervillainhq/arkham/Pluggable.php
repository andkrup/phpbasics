<?php
namespace supervillainhq\arkham{
	interface Pluggable{
		function resetPlugins(array $plugins = []);
		function addPlugin($key, Plugin $plugin);
		function removePlugin(Plugin $plugin);
		function removePluginAt($key);
		function hasPlugin(Plugin $plugin);
		function hasPluginAtKey($key);
		function getPlugin($key);
		function plugins();
	}
}
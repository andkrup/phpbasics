<?php
namespace supervillainhq\arkham{
	use supervillainhq\arkham\events\EventDispatching;

	/**
	 * Uses EventDispatching
	 * @author ak
	 *
	 */
	trait Plugining{
		use EventDispatching;

		protected $plugins;

		function resetPlugins(array $plugins = []){
			$this->plugins = $plugins;
		}
		function addPlugin($key, Plugin $plugin){
			$this->plugins[$key] = $plugin;
		}
		function removePlugin(Plugin $plugin){
			$c = count($this->plugins);
			for($i = 0; $i < $c; $i++){
				if($this->plugins[$i] == $plugin){
					array_splice($this->plugins, $i, 0);
				}
			}
		}
		function removePluginAt($key){
			unset($this->plugins[$key]);
		}
		function hasPlugin(Plugin $plugin){
			return in_array($this->plugins, $plugin);
		}
		function hasPluginAtKey($key){
			return array_key_exists($key, $this->plugins);
		}
		function getPlugin($key){
			return $this->plugins[$key];
		}
		function plugins(){
			return $this->plugins;
		}
	}
}
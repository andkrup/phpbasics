<?php
namespace supervillainhq\arkham\cache{
	/**
	 * Proxy for the cache-engine best used for caching page objects
	 *
	 * @author ak
	 *
	 */
	class PageCache implements Cache{
		function get($key){}
		function set($key, $value){}
		function exists($key){}
		function getHash($key){}
	}
}
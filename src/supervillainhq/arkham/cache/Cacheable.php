<?php
namespace supervillainhq\arkham\cache{
	interface Cacheable{
		function cacheGroup($cacheGroup = null);
		function cacheKey($cacheKey = null);
		function cacheBy($cacheKey = null, $cacheGroup = null);
	}
}
<?php
namespace supervillainhq\arkham\cache{

	class CacheManager{
		protected $tree;

		function __construct(){
			// fetch/build a tree-structure that contains all the nodes and relations concerning what cached values are up-to-date
			$this->tree = null;
		}

		/**
		 * Invalidate all cache-keys that belongs to a cache-group (nested tree-nodes)
		 * @param string $group
		 * @param $query
		 */
		function invalidateGroup($group, $query){
			$nodes = $this->fetchNodes($group);
			foreach ($nodes as $key => $node){
				$this->rebuildNode($node, null);
			}
		}
		/**
		 * Invalidate a specific node
		 * @param string $key
		 * @param $query
		 */
		function invalidateKey($key, $query){
			$node = $this->fetchNode($key);
			return $this->rebuildNode($node, null);
		}
		/**
		 * Rebuild the complete tree
		 */
		function rebuild(){}
		protected function rebuildNode($node, $value){
			return false;
		}

		/**
		 * Returns a single node
		 * @param string $key
		 */
		function fetchNode($key){}
		/**
		 * Returns a node and its children
		 * @param string $key
		 * @param boolean $deep
		 */
		function fetchNodes($key, $deep = false){}
		function fetchParent($key){}
		function fetchParents($key, $deep = false){}
		function fetchSiblings($key){}
		function fetchChildren($key){}
	}
}
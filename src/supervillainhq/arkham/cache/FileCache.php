<?php
namespace supervillainhq\arkham\cache{
	class FileCache implements Cache{
		protected $cacheDir;

		function cacheDir($path = null){
			if(is_null($path)){
				return $this->cacheDir;
			}
			$this->cacheDir = $path;
		}

		public function __construct($cacheDir){
			$this->cacheDir($cacheDir);
		}

		function get($key){}
		function set($key, $value){}
		function exists($key){}

		function getHash($key){
			$value = $this->get($key);
			if(is_array($value)){
				return md5(implode(';', $value));
			}
		}
	}
}
?>
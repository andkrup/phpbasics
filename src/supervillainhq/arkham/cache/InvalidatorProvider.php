<?php
namespace supervillainhq\arkham\cache{
	class InvalidatorProvider{
		/**
		 *  TODO: this factory method should be able to return either a slim frontendinvalidator
		 *  or a full invalidator that also can modify cache values
		 * @return Invalidator an Invalidator instance
		 */
		static function current(){
			return null;
		}
	}
}
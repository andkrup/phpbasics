<?php
namespace supervillainhq\arkham\cache{

	/**
	 * This manager determines whether queries should be sent to execution on the database server,
	 * or if cached data will suffice.
	 * We would like to know if the requested data is stale or useful and the 2 cases in which data
	 * is stale is when input parameters are dynamic or if an update-action has altered the data
	 *
	 * 1) Dynamic input (shorter-term cache strategy)
	 *    Dynamic data could be impractical to cache, unless we can ensure that we don't blow memory
	 *    usage through the roof. It may be something in the way of having a TTL on the cached data
	 *    (within session-scope)
	 *
	 * 2) Updated data (longer-term cache strategy)
	 *    Update actions should always invalidate the cache-identifier so data that seldom change
	 *    can be treated as static cache
	 *
	 * This invalidator should access a shared in-memory tree that holds information about what
	 * cache values are updated
	 *
	 * @author ak
	 *
	 */
	class RedisCacheManager extends CacheManager{
		function invalidateGroup($group, $query){}
		function invalidateKey($key, $query){}
	}
}
<?php
namespace supervillainhq\arkham\cache{
	interface Cache{
		function get($key);
		function set($key, $value);
		function exists($key);
		function getHash($key);
	}
}
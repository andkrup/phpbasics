<?php
namespace supervillainhq\arkham\cache{
	/**
	 * Contract for frontend invalidators that can fetch cached values and
	 * query the cache storage if values are stale or not
	 * @author ak
	 *
	 */
	interface FrontendInvalidator{
		function isStale($cacheKey);
	}
}
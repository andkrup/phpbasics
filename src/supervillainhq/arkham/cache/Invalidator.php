<?php
namespace supervillainhq\arkham\cache{
	/**
	 * Contract for invalidators that also can access and modify cachevalues, useful
	 * in the backend
	 * @author ak
	 *
	 */
	interface Invalidator extends FrontendInvalidator{
		/**
		 * Update the cached values and notify interested parties
		 * @param string $cacheKey
		 * @param string $cacheGroup
		 */
		function invalidate($cacheKey, $cacheGroup = null);
	}
}
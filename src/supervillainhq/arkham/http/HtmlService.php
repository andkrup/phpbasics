<?php
namespace supervillainhqarkham\http{
	use supervillainhq\core\Factory;
	use supervillainhqarkham\http\routing\events\RouteEvent;
	use supervillainhqarkham\http\routing\GetRequest;
	use supervillainhqarkham\http\events\ControllerEvent;
	use supervillainhqarkham\http\events\HtmlServiceEvent;

	class HtmlService extends HttpService implements Service{
		protected $getRequest;
		protected $headers;
		protected $silent;

		function currentGetRequest(){
			return $this->getRequest;
		}
		/**
		 * Print all http headers
		 */
		function headers(){
		}

		function loadData(){
			$event = new HtmlServiceEvent($this, HtmlServiceEvent::LOAD_DATA);
			$this->dispatchEvent($event);
		}
		function render(){
			$event = new HtmlServiceEvent($this, HtmlServiceEvent::RENDER);
			$this->dispatchEvent($event);
		}

		function __construct($silent = false){
			parent::__construct();
			$this->silent = $silent;
		}

		function onRouteEvent(RouteEvent $event){
			if(isset($event)){
				$request = $event->request();
				if($request instanceof GetRequest){
					$this->getRequest = $request;
					$controllerFragmentKey = 'controller';
					$actionFragmentKey = 'action';
					$controllerFragment = $this->getRequest->getFragment($controllerFragmentKey);
					$actionFragment = $this->getRequest->getFragment($actionFragmentKey);
					if(is_null($actionFragment)){
						$actionFragment = 'index';
					}
					$provider = ApplicationProvider::instance();
					if(!isset($controllerFragment)){
						$controllerFragment = 'index';
					}
					$controllerFactory = $provider->getProvider('controllerFactory', $controllerFragment, $actionFragment);
					if(isset($controllerFactory) && $controllerFactory instanceof Factory){
						$controller = $controllerFactory->create();
						if(isset($controller)){
							$this->addListener($controller, HtmlServiceEvent::LOAD_DATA);
							$this->addListener($controller, HtmlServiceEvent::RENDER);
							$event = new ControllerEvent($controller, ControllerEvent::READY);
							$this->dispatchEvent($event);
							$methodName = $this->methodName($actionFragment);
							if(method_exists($controller, $methodName)){
								$controller->$methodName();
							}
							else{
								if(!$this->silent){
									throw new \Exception("Controller '{$controllerFragment}' does not define action '{$methodName}'");
								}
							}
						}
					}
				}
			}
		}
		private function methodName($actionFragment){
// 			preg_match('/[-_ .,\+]([\w])/', $actionFragment, $matches);
			$method = preg_replace('/[-_ .,\+]/', ' ', $actionFragment);
			$method = trim($method);
			$method = ucwords($method);
			$method = str_replace(" ", "", $method);
			return lcfirst($method);
		}
	}
}
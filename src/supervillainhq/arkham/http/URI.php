<?php
namespace supervillainhq\arkham\http{
	final class URI{
		private $original;
		private $hash;

		function __construct($string){
			$this->original = $string;
			$this->hash = (object) parse_url($string);
		}

		public function host(){
			if(isset($this->hash)){
				return $this->hash->host;
			}
		}
		public function port(){
			if(isset($this->hash)){
				return $this->hash->port;
			}
		}
		public function user(){
			if(isset($this->hash)){
				return $this->hash->user;
			}
		}
		public function pass(){
			if(isset($this->hash)){
				return $this->hash->pass;
			}
		}
		public function path(){
			if(isset($this->hash)){
				return $this->hash->path;
			}
		}
		public function query(){
			if(isset($this->hash)){
				return $this->hash->query;
			}
			return null;
		}
		public function fragment(){
			if(isset($this->hash)){
				return $this->hash->fragment;
			}
			return null;
		}
	}
}
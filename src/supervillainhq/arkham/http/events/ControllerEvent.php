<?php
namespace supervillainhq\arkham\http\events{
	use supervillainhq\arkham\events\Event;
	use supervillainhq\arkham\http\Controller;

	class ControllerEvent extends Event{
		const CREATE = 'controllerCreate';
		const READY = 'controllerReady';

		protected $controller;

		function __construct(Controller $controller, $type = null){
			if(is_null($type)){
				$type = self::CREATE;
			}
			parent::__construct($type);
			$this->controller = $controller;
		}

		function controller(){
			return $this->controller;
		}
	}
}
<?php
namespace supervillainhq\arkham\http\events{
	use supervillainhq\arkham\events\Event;
	use supervillainhq\arkham\http\Service;

	class HtmlServiceEvent extends Event{
		const LOAD_DATA = 'serviceLoadData';
		const RENDER = 'serviceRender';

		protected $service;

		function __construct(Service $service, $type = null){
			if(is_null($type)){
				$type = self::CREATE;
			}
			parent::__construct($type);
			$this->service = $service;
		}

		function service(){
			return $this->service;
		}
	}
}
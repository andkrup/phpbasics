<?php
namespace supervillainhq\arkham\http\events{
	use supervillainhq\arkham\events\Event;

	class ApplicationProviderEvent extends Event{
		const GET = 'providerGet';

		protected $providerKey;

		function __construct($providerKey, $type = null){
			if(is_null($type)){
				$type =self::GET;
			}
			parent::__construct($type);
			$this->providerKey = $providerKey;
		}

		function providerKey(){
			return $this->providerKey;
		}
	}
}
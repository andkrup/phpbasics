<?php
namespace supervillainhqarkham\http{
	use supervillainhq\arkham\Plugining;

	class HttpService implements Service{
		use Plugining;

		function __construct(){
			$this->resetListeners();
			$this->resetPlugins();
		}

	}
}
<?php
/**
 * Created by PhpStorm.
 * User: ak
 * Date: 26/03/16
 * Time: 22:34
 */

namespace supervillainhq\arkham\http\html{

	class Option {
		private static $template = <<<TEMPLATE
<option value=":value":selected>:title</option>
TEMPLATE;

		static function items(array $items, $selectedValue, $titleGetter = 'name', $valueGetter = 'id'){
			$html = [];
			foreach ($items as $item){
				$selected = '';
				$uVal = $item->{$valueGetter}();
				$uTitle = $item->{$titleGetter}();

				$option = str_replace(':value', $uVal, self::$template);
				$option = str_replace(':title', $uTitle, $option);
				if($selectedValue == $uVal){
					$selected = ' selected="selected"';
				}
				array_push($html, str_replace(':selected', $selected, $option));
			}
			return implode("\n", $html);
		}
	}
}
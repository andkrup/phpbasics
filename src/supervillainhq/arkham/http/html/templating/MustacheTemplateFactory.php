<?php
namespace supervillainhq\arkham\http\html\templating{
	class MustacheTemplateFactory{
		protected $baseDir;
		protected $templatePath;

		/**
		 *
		 * @param unknown $baseDir absolute path to template directory
		 * @param string $templatePath filename/path relative to baseDir
		 * @throws \Exception
		 */
		function __construct($baseDir, $templatePath){
			if(!class_exists("\Mustache_Engine")){
				throw new \Exception('Please install the Mustache dependency');
			}
			$this->baseDir = $baseDir;
			$this->templatePath = $templatePath;
		}
		function create(){
			$templateLoader = new PhpBasicsMustacheLoader($this->baseDir);
			return new MustacheTemplate($this->templatePath, $templateLoader);
		}
	}
}
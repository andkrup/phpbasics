<?php
namespace supervillainhq\arkham\http\html\templating{
	trait Templated{
		protected $filepath;

		function filepath($filepath = null){
			if(is_null($filepath)){
				return $this->filepath;
			}
			$this->filepath = $filepath;
		}
	}
}
<?php
namespace supervillainhq\arkham\http\html\templating{
	class MustacheTemplate implements Template{
		use Templated;

		protected $engine;
		protected $templateLoader;
		protected $partialsLoader;
		protected $data;
		protected $filename;
		protected $fileContents;
		protected $placeholders;

		private function fileContents(){
			if(!isset($this->fileContents)){
				$this->fileContents = file_get_contents($this->filepath);
			}
			return $this->fileContents;
		}

		function __construct($filename, MustacheTemplateLoader $templateLoader, MustacheTemplateLoader $partialsLoader = null){
			$this->filename = $filename;
			$this->templateLoader = $templateLoader;
			$this->partialsLoader = $partialsLoader;
			$config = [];
			if(!is_null($templateLoader)){
				$config['loader'] = $templateLoader;
			}
			if(!is_null($partialsLoader)){
				$config['partials_loader'] = $partialsLoader;

			}
			$this->engine = new \Mustache_Engine($config);
			$filepath = "{$this->templateLoader->templatePath()}/{$filename}";
			$this->filepath($filepath);
		}

		function load($data = null){
			$this->data = $data;
		}

		function placeholders(){
			if(isset($this->placeholders)){
				return $this->placeholders;
			}
			$this->placeholders = [];
			self::parseTokens($this->filepath, $this->placeholders);
			return $this->placeholders;
		}
		private static function parseTokens($filepath, array &$placeholders){
			$context = pathinfo($filepath, PATHINFO_FILENAME);
			$fileContents = file_get_contents($filepath);
			$tokenizer = new \Mustache_Tokenizer();
			$tokens = $tokenizer->scan($fileContents);
			foreach ($tokens as $token){
				$tokenType = $token[\Mustache_Tokenizer::TYPE];
				switch($tokenType){
					case \Mustache_Tokenizer::T_TEXT:
						break;
					case \Mustache_Tokenizer::T_PARTIAL:
						$tokenValue = $token[\Mustache_Tokenizer::NAME];
						$dir = pathinfo($filepath, PATHINFO_DIRNAME);
						self::parseTokens("{$dir}/{$tokenValue}.html", $placeholders);
						// MISSING BREAK INTENTED! FOR DRIPPING DOWN TO NEXT CASE!
					default:
						$tokenValue = $token[\Mustache_Tokenizer::NAME];
						$tag = [
							'name' => $tokenValue,
							'token' => $tokenType,
							'type' => self::getTokenType($tokenType),
							'scope' => $context,
						];
						array_push($placeholders, (object) $tag);
						break;
				}
			}
		}
		private static function getTokenType($value){
			switch($value){
				case \Mustache_Tokenizer::T_ESCAPED:
					return 'escaped';
				case \Mustache_Tokenizer::T_UNESCAPED:
				case \Mustache_Tokenizer::T_UNESCAPED_2:
					return 'unescaped';
				case \Mustache_Tokenizer::T_PARTIAL:
					return 'partial';
				case \Mustache_Tokenizer::T_INVERTED:
					return 'invertedsection';
				case \Mustache_Tokenizer::T_SECTION:
					return 'section';
				case \Mustache_Tokenizer::T_END_SECTION:
					return 'endsection';
				default:
					return $value;
			}
		}

		function render(){
			return $this->engine->render($this->filename, $this->data);
		}
	}
}
<?php
namespace supervillainhq\arkham\http\html\templating{
	interface MustacheTemplateLoader extends \Mustache_Loader{
		function templatePath($templatePath = null);
	}
}
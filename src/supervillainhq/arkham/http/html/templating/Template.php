<?php
namespace supervillainhq\arkham\http\html\templating{
	interface Template{
		function filepath($filepath = null);
		function load($data = null);
		function placeholders();
		function render();
	}
}
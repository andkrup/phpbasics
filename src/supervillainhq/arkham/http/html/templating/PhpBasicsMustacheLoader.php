<?php
namespace supervillainhq\arkham\http\html\templating{
	class PhpBasicsMustacheLoader implements MustacheTemplateLoader{
		protected $templatePath;
		protected $options;

		function templatePath($templatePath = null){
			if(!is_null($templatePath)){
				$this->templatePath = $templatePath;
			}
			return $this->templatePath;
		}

		function __construct($templatePath, array $options = null){
			$this->templatePath = $templatePath;
			if(is_null($options)){
				$options = [
						'extension' => 'html',
				];
			}
			$this->options = $options;
		}

		public function load($name){
			$proxiedLoader = new \Mustache_Loader_FilesystemLoader($this->templatePath, $this->options);
			return $proxiedLoader->load($name);
		}
	}
}
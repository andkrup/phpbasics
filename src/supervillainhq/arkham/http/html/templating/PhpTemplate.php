<?php
namespace supervillainhq\arkham\http\html\templating{

	class PhpTemplate implements Template{
		use Templated;

		function load($data = null){}
		function placeholders(){
			return [];
		}
		function render(){}
	}
}
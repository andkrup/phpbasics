<?php
namespace supervillainhqarkham\http{
	class HttpHeader{
		protected $name;
		protected $value;

		function name($name = null){
			if(is_null($name)){
				return $this->name;
			}
			$this->name = $name;
		}

		function value($value = null){
			if(is_null($value)){
				return $this->value;
			}
			$this->value = $value;
		}
	}
}
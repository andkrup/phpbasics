<?php
namespace supervillainhq\arkham\http{
	use supervillainhq\arkham\Plugin;

	/**
	 * A Service describes something that accepts a request
	 * and returns formatted data back. This could be something
	 * like the HtmlService that interprets a request and returns data formatted as html
	 * @author ak
	 *
	 */
	interface Service extends Plugin{}
}
<?php
namespace supervillainhq\arkham\http{
	use supervillainhq\arkham\http\routing\Router;
	use supervillainhq\arkham\http\routing\events\RouteEvent;
	use supervillainhq\arkham\Bootstrap;
	use supervillainhq\arkham\ClassLoader;
	use supervillainhq\arkham\Plugin;
	use supervillainhq\arkham\Plugining;
	use supervillainhq\core\env\Environment;

	class Site implements Plugin{
		use Plugining;

		private static $instance;
		protected $approot;
		protected $host;
		protected $environment;
		protected $services;

		static function instance(){
			if(!isset(self::$instance)){
				self::$instance = new Site();
			}
			return self::$instance;
		}

		function docroot(){
			$path_parts = pathinfo($_SERVER['PHP_SELF']);
			return $path_parts['dirname'];
		}
		function approot($path = null){
			if(is_null($path)){
				return $this->approot;
			}
			$this->approot = $path;
		}


		private function __construct(){
			$this->environment = new Environment();
			$this->host = $_SERVER['HTTP_HOST'];
			$router = Router::instance();
			$router->addListener($this, RouteEvent::ROUTE);
		}

		function onRouteEvent(RouteEvent $event){
		}


		public static function currentTemplate(){
			global $templateConfig;
			if(isset($templateConfig)){
				return $templateConfig['current'];
			}
			return null;
		}
		public static function templatePath($subPath = null){
			global $templateConfig;
			if(isset($templateConfig)){
				global $appRoot;
				$template = $templateConfig['current'];
				$dirname = $templateConfig['dirname'];
				if(isset($subPath)){
					return "$appRoot/$dirname/$template/$subPath";
				}
				else{
					return "$appRoot/$dirname/$template";
				}
			}
			return null;
		}

		/**
		 * PRE: configs should have been loaded
		 *
		 * @param Bootstrap $bootstrap
		 */
		function bootstrap(Bootstrap $bootstrap, $appCfg){
			// keep a reference to the approot path
			$paths = $bootstrap->paths();
			$this->approot = $paths[Bootstrap::APP_PATH];
			// set up source paths for the classloader
			$sourcePaths = $appCfg->sources;
			if(isset($sourcePaths)){
				foreach ($sourcePaths as $path){
					$absPath = $this->absPath($path);
					ClassLoader::addSourcePath($absPath);
				}
			}
		}

		function absPath($relativePath){
			return "{$this->approot}/{$relativePath}";
		}

		function resetServices(array $services = []){
			$this->services = $services;
		}
		/**
		 * Currently we dont consider the keys used, but they could probably look like:
		 * 'html' or 'json'
		 *
		 * @param string $key
		 * @param Service $service
		 */
		function addService($key, Service $service){
			$router = Router::instance();
			$router->addListener($service, RouteEvent::ROUTE);
			$this->addListener($service, RouteEvent::ROUTE);
			$this->services[$key] = $service;
		}
		function removeService(Service $service){
			$router = Router::instance();
			$c = count($this->services);
			foreach($this->services as $key => $srv){
				if($srv == $service){
					$this->services[$key] = null;
					$router->removeListener($service, RouteEvent::ROUTE);
					$this->removeListener($service, RouteEvent::ROUTE);
				}
			}
		}
		function removeServiceAt($key){
			unset($this->services[$key]);
		}
		function hasService(Service $service){
			return in_array($this->services, $service);
		}
		function hasServiceAtKey($key){
			return array_key_exists($key, $this->services);
		}
		function getService($index){
			return $this->services[$index];
		}
		function services(){
			return $this->services;
		}

	}
}
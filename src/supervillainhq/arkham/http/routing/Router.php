<?php
namespace supervillainhq\arkham\http\routing{
	use supervillainhq\arkham\events\Dispatcher;
	use supervillainhq\arkham\events\EventDispatching;
	use supervillainhq\arkham\http\routing\events\RouteEvent;

	/**
	 * Requires the core phar (implements the Dispatcher interface from the core phar)
	 *
	 * @author ak
	 *
	 */
	class Router implements Dispatcher{
		use EventDispatching;

		const METHOD_GET = 'get';
		const METHOD_POST = 'post';
		const METHOD_PUT = 'put';
		const METHOD_DELETE = 'delete';

		protected static $instance;
		protected $requests;

		static function instance(){
			if(!isset(self::$instance)){
				self::$instance = new Router();
			}
			return self::$instance;
		}

		private function __construct(){
			$this->resetListeners();
			$this->resetRequests();
		}

		/**
		 * Creates request objects
		 *
		 * @return \supervillainhq\arkham\http\routing\Router
		 */
		static function route(){
			$instance = self::instance();
			// always create and add a GetRequest
			$request = new GetRequest();
			$instance->addRequest(self::METHOD_GET, $request);
			// only create and add a PostRequest if needed
			if(self::METHOD_POST == $_SERVER['REQUEST_METHOD'] && count($_POST)>0){
				$instance->addRequest(self::METHOD_POST, new PostRequest());
			}
			// TODO: do we need to be able to hook into here in order to add additional requests?
			return $instance;
		}

		/**
		 * Dispatches request objects to listeners
		 */
		function handleRequests(){
			foreach ($this->requests as $request){
				$request->parse();
				$event = new RouteEvent($request);
				$this->dispatchEvent($event);
			}
		}

		function resetRequests(array $requests = []){
			$this->requests = $requests;
		}
		function addRequest($key, Request $request){
			$this->requests[$key] = $request;
		}
		function removeRequest(Request $request){
			$c = count($this->requests);
			for($i = 0; $i < $c; $i++){
				if($this->requests[$i] == $request){
					array_splice($this->requests, $i, 0);
				}
			}
		}
		function removeRequestAt($key){
			unset($this->requests[$key]);
		}
		function hasRequest(Request $request){
			return in_array($this->requests, $request);
		}
		function hasRequestAtKey($key){
			return array_key_exists($key, $this->requests);
		}
		function getRequest($index){
			return $this->requests[$index];
		}
		function requests(){
			return $this->requests;
		}
	}
}
<?php
namespace supervillainhq\arkham\http\routing\events{
	use supervillainhq\arkham\events\Event;
	use supervillainhq\arkham\http\routing\Request;

	class RouteEvent extends Event{
		const ROUTE = 'routeEvent';

		protected $request;

		function __construct(Request $request, $type = null){
			if(is_null($type)){
				$type = self::ROUTE;
			}
			parent::__construct($type);
			$this->request = $request;
		}

		function request(){
			return $this->request;
		}
	}
}
<?php
namespace supervillainhq\arkham\http\routing{
	class Request{
		protected $parameters;

		function __construct(array $parameters){
			$this->resetParameters($parameters);
		}


		function parse(){
		}

		function resetParameters(array $parameters = []){
			$this->parameters = $parameters;
		}
		function addParameter($key, $parameter){
			$this->parameters[$key] = $parameter;
		}
		function removeParameter($parameter){
			$c = count($this->parameters);
			for($i = 0; $i < $c; $i++){
				if($this->parameters[$i] == $parameter){
					array_splice($this->parameters, $i, 0);
				}
			}
		}
		function removeParameterAt($key){
			unset($this->parameters[$key]);
		}
		function hasParameter($parameter){
			return in_array($this->parameters, $parameter);
		}
		function hasParameterAtKey($key){
			return array_key_exists($key, $this->parameters);
		}
		function getParameter($index){
			return $this->parameters[$index];
		}
		function parameters(){
			return $this->parameters;
		}
	}
}
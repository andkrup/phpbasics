<?php
namespace supervillainhq\arkham\http\routing{

	class GetRequest extends Request{
		protected $patterns;
		protected $matches;

		function __construct(array $patterns = null){
			parent::__construct($_GET);
			if(is_null($patterns)){
				$patterns = ["/controller", "/controller/action", "/module/controller/action"];
			}
			$this->patterns = $patterns;
		}
		/**
		 * Return the fragment of a pattern that has been matched in the request uri.
		 *
		 * Example:
		 *   If the  GetRequest object has been created with the pattern '/controller/action'
		 *   then it can match up the given request uri 'http://foo.com/bar/baz' so that it
		 *   knows that the fragment that is called 'controller' matches to the value 'bar'
		 *
		 * @param string $fragment
		 * @return NULL|string
		 */
		function getFragment($fragment){
			if(!isset($this->matches)){
				return null;
			}
			$c = count($this->matches);
			if(0 < $c && $c < count($this->patterns)){
				$pattern = $this->patterns[$c-1];
				$frags = explode('/', substr($pattern, 1));
				$index = array_search($fragment, $frags);
				if(false===$index){
					return null;
				}
				return $this->matches[$index];
			}
			return null;
		}

		function parse(){
			parent::parse();
			$uri = $_SERVER['REQUEST_URI'];
			$info = pathinfo($uri);
			$frags = explode('/', substr($info['dirname'], 1));
			$file = $info['filename'];
			if(false!==strpos($file, '?')){
				$file = substr($file, 0, strpos($file, '?'));
			}
			array_push($frags, $file);
			$this->matches = [];
			foreach ($frags as $frag){
				if(strlen($frag)>0){
					array_push($this->matches, $frag);
				}
			}
// 			$this->matches = $frags;

// 			$availableKeys;
// 			$keys = func_get_args();
// 			if(count($keys)>0){
// 				$availableKeys = array_intersect($keys, array_keys($_GET));
// 			}
// 			else{
// 				$availableKeys = array_keys($_GET);
// 				if(count($availableKeys)<1){
// 					array_push($availableKeys, 'default'); // add the key 'default' if no other keys are available so there is a way to add a handler for null-key scenarios
// 				}
// 			}
// 			if(!in_array('/', $availableKeys)){
// 				array_push($availableKeys, '/'); // always add the key '/' so there is some way to invoke an 'always-present' handler
// 			}
// 			foreach ($availableKeys as $key){
// 				if($key == '' || $key == 'default'){
// 					if(array_key_exists('default', $this->handlers)){
// 						$defaultHandler = $this->handlers['default'];
// 					}
// 					if(isset($defaultHandler)){
// 						$defaultHandler->get();
// 						$index = 'index';
// 						$defaultHandler->$index();
// 						$this->buffer = $defaultHandler->buffer() . $this->buffer;
// 					}
// 				}
// 				elseif(array_key_exists($key, $this->handlers)){
// 					$currentHandler = $this->handlers[$key];
// 					if(isset($currentHandler)){
// 						$method = array_key_exists($key, $_GET) ? trim($_GET[$key]) : null;
// 						if(!is_null($method) && method_exists($currentHandler, $method)){
// 							$currentHandler->$method();
// 						}
// 						else{
// 							$currentHandler->get();
// 						}
// 						$this->buffer = $currentHandler->buffer() . $this->buffer;
// 					}
// 				}
// 			}
		}

		function resetPatterns(){
			$this->patterns = [];
		}
		function addPattern($pattern){
			array_push($this->patterns, $pattern);
		}
		function removePattern($pattern){
			$c = count($this->patterns);
			for($i = 0; $i < $c; $i++){
				if($this->patterns[$i] == $pattern){
					array_splice($this->patterns, $i, 0);
				}
			}
		}
		function hasPattern($pattern){
			return in_array($this->patterns, $pattern);
		}
		function getPattern($index){
			return $this->patterns[$index];
		}
		function patterns(){
			return $this->patterns;
		}
	}
}
?>
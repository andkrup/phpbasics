<?php
namespace supervillainhqarkham\http{
	use supervillainhqarkham\http\routing\events\RouteEvent;

	class JsonService extends HttpService implements Service{

		function onRouteEvent(RouteEvent $event){
			echo "JsonService->onRouteEvent()";
		}
	}
}
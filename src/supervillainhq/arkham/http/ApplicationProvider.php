<?php
namespace supervillainhqarkham\http{
	use supervillainhq\arkham\http\events\ApplicationProviderEvent;
	use supervillainhq\arkham\events\Dispatcher;
	use supervillainhq\arkham\events\EventDispatching;

	class ApplicationProvider implements Dispatcher{
		use EventDispatching;

		private static $instance;
		protected $closures;

		static function instance(){
			if(!isset(self::$instance)){
				self::$instance = new static();
			}
			return self::$instance;
		}

		private function __construct(){}


		function addProvider($key, callable $closure){
			return $this->addClosure($key, $closure);
		}
		function getProvider($key){
			$event = new ApplicationProviderEvent($key);
			$this->dispatchEvent($event);
			$closure = $this->closures[$key];
			if(is_callable($closure)){
				if(func_num_args()>0){
					$args = func_get_args();
					$args = array_splice($args, 1);
					return call_user_func_array($closure, $args);
				}
				return $closure();
			}
			throw new \Exception("no provider found for '{$key}'");
		}

		function resetClosures(array $closures = []){
			$this->closures = $closures;
		}
		function addClosure($key, callable $closure){
			$this->closures[$key] = $closure;
		}
		function removeClosure(callable $closure){
			$c = count($this->closures);
			for($i = 0; $i < $c; $i++){
				if($this->closures[$i] == $closure){
					array_splice($this->closures, $i, 0);
				}
			}
		}
		function removeClosureAt($key){
			unset($this->closures[$key]);
		}
		function hasClosure(callable $closure){
			return in_array($this->closures, $closure);
		}
		function hasClosureAtKey($key){
			return array_key_exists($key, $this->closures);
		}
		function getClosure($index){
			return $this->closures[$index];
		}
		function closures(){
			return $this->closures;
		}
	}
}
?>
<?php
namespace supervillainhq\arkham\events{
	trait EventDispatching{
		protected $listeners;


		function dispatchEvent(Event $event){
			if(!is_array($this->listeners)){
				return false;
			}
			$eventType = $event->type();
			if(array_key_exists($eventType, $this->listeners)){
				$listeners = $this->listeners[$eventType];
				if(is_array($listeners) && 0<count($listeners)){
					foreach ($listeners as $listener){
						$callable = "on".ucfirst($eventType);

						if(method_exists($listener, $callable)){
							$listener->$callable($event);
						}
						else{
							throw new \Exception("listener does not implement callback '{$callable}'.");
						}
					}
					return true;
				}
			}
			return false;
		}

		function resetListeners(){
			$this->listeners = [];
		}
		function addListener(Listener $listener, $event){
			if(!is_array($this->listeners)){
				$this->resetListeners();
			}
			if(!array_key_exists($event, $this->listeners) || !is_array($this->listeners[$event])){
				$this->listeners[$event] = [];
			}
			array_push($this->listeners[$event], $listener);
		}
		function removeListeners($event){
			$this->listeners[$event] = [];
		}
		function removeListener(Listener $listener, $event){
			$arr = &$this->listeners[$event];
			if(is_array($arr)){
				$c = count($arr);
				for($i = 0; $i < $c; $i++){
					if($arr[$i] == $listener){
						array_splice($arr, $i, 0);
					}
				}
			}
		}
		function hasListeners($event){
			if(is_array($this->listeners[$event])){
				return count($this->listeners[$event]) > 0;
			}
			return false;
		}
		function hasListener(Listener $listener, $event){
			if(is_array($this->listeners[$event])){
				return in_array($this->listeners[$event], $listener);
			}
			return false;
		}
		function getListeners($event){
			if(is_array($this->listeners[$event])){
				return $this->listeners[$event];
			}
			return null;
		}
		function getListener($event, $index){
			$arr = &$this->listeners[$event];
			if(is_array($arr)){
				return $arr[$index];
			}
			return null;
		}
		function listeners(){
			return $this->listeners;
		}
	}
}
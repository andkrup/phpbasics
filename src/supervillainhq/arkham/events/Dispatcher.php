<?php
namespace supervillainhq\arkham\events{
	interface Dispatcher{
		function addListener(Listener $listener, $event);
		function removeListeners($event);
		function removeListener(Listener $listener, $event);
		function hasListeners($event);
		function hasListener(Listener $listener, $event);
		function getListeners($event);
		function getListener($event, $index);
		function listeners();
		function dispatchEvent(Event $event);
	}
}
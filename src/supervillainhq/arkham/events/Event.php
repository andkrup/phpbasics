<?php
namespace supervillainhq\arkham\events{
	class Event{
		protected $eventType;

		function __construct($type = 'event'){
			$this->eventType = $type;
		}

		function type($type = null){
			if(is_null($type)){
				return $this->eventType;
			}
			$this->eventType = $type;
		}
	}
}
<?php
namespace supervillainhq\arkham\validation{
	class TextValidator implements Validator{
		protected $requiredLength;

		public function __construct($requiredLength = 2){
			$this->requiredLength = $requiredLength;
		}

		public function validate($string){
			return strlen(trim($string)) >= $this->requiredLength;
		}
	}
}
?>
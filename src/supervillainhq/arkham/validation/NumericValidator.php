<?php
namespace supervillainhq\arkham\validation{
	class NumericValidator extends TextValidator implements Validator{
		public function validate($string){
			if(false==parent::validate($string)){
				return false;
			}
			return is_numeric($string);
		}
	}
}
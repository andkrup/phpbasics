<?php
namespace supervillainhq\arkham\validation{
	class EmailValidator extends TextValidator implements Validator{
		private static $regexp = "/^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$/";
		public function __construct(){
			parent::__construct(5);
		}

		public function validate($string){
			if(false==parent::validate($string)){
				return false;
			}
			if(preg_match(self::$regexp, trim($string))){
				return true;
			}
			return false;
		}
	}
}
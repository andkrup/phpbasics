<?php
namespace supervillainhq\arkham\server{
	use supervillainhq\core\Command;
	
	/**
	 * Currently only supports bash redirection/pipes
	 *
	 * @author zr-ank
	 *
	 */
	class OpenProcess implements Command{
		private $procid;
		private $terminated;
		private $logfilepath;
		private $_returnValue;
		private $commandFilepath;
		private $filename;
		private $arguments;

		public function __construct($commandFilepath, array $args = null, $logfilepath = '/tmp/openprocess.log'){
			$this->commandFilepath = escapeshellarg($commandFilepath);
			$this->filename = pathinfo($this->commandFilepath,  PATHINFO_FILENAME);
			$this->arguments = array();
			$this->logfilepath = $logfilepath;

			if(!is_null($args)){
				foreach ($args as $arg){
					array_push($this->arguments, escapeshellarg($arg));
				}
			}
		}

		public function execute(){
			$cmd = $this->commandFilepath;

			for ($i = 0; $i < count($this->arguments); $i++){
				$cmd .= " {$this->arguments[$i]}";
			}

			$op;
			if(!file_exists($this->logfilepath)){
				touch($this->logfilepath);
			}
			file_put_contents($this->logfilepath, "\n= NEW PROCESS =\n", FILE_APPEND);
			if(function_exists(apache_request_headers)){
				$headers = apache_request_headers();
				$headers = var_export($headers, true);
				file_put_contents($this->logfilepath, "\nHeaders:\n$headers\n", FILE_APPEND);
			}
			else{
				$dump = var_export($_SERVER, true);
				file_put_contents($this->logfilepath, "\nHeaders (fast-cgi):\n$dump\n", FILE_APPEND);
			}
			if(function_exists(http_get_request_body)){
				$body = http_get_request_body();
				$body = var_export($body, true);
				file_put_contents($this->logfilepath, "\nRequest:\n$body\n", FILE_APPEND);
			}
	// 		exec(sprintf("%s > %s 2>&1 & echo $! >> %s", $cmd, $outputfile, $pid));
			exec(sprintf("%s >> %s 2>&1 & echo $!", $cmd, $this->logfilepath), $op);

	// 		$cmd = "$cmd > $outputfile 2>&1 & echo $!";
	// 		echo "command[$cmd]";
	// 		exec($cmd, $op);

	// 		exec(sprintf("%s > %s 2>&1 & echo $!", $cmd, '/dev/null'), $op);
			$this->pid = (int) $op[0];
			return $this->pid;
		}

	// 	public function output(){
	// 		return $this->_output;
	// 	}
	// 	public function returnValue(){
	// 		return $this->_returnValue;
	// 	}

		public static function status($pid){
			$command = 'ps -p '.$pid;
			exec($command,$op);
			if (!isset($op[1]))return false;
			else return true;
		}
	}
}
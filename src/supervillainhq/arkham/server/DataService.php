<?php
namespace supervillainhq\arkham\server{
	use supervillainhq\arkham\http\Service;
	use supervillainhq\arkham\Plugining;

	class DataService implements Service{
		use Plugining;

		function __construct(){
			$this->resetListeners();
			$this->resetPlugins();
		}
	}
}
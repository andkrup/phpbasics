<?php
namespace supervillainhq\arkham{
	use supervillainhq\arkham\events\Listener;
	use supervillainhq\arkham\events\Dispatcher;

	interface Plugin extends Listener, Dispatcher{
	}
}
?>
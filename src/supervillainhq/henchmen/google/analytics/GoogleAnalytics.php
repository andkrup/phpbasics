<?php
namespace supervillainhq\henchmen\google\analytics{
	class GoogleAnalytics{
		private static $jsScript = <<<EOT
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', ':userId', 'auto');
ga('send', 'pageview');
EOT;
		private $userId;

		function __construct($userId){
			$this->userId = $userId;
		}

		function getScript(){
			return str_replace(':userId', trim($this->userId), self::$jsScript);
		}
	}
}
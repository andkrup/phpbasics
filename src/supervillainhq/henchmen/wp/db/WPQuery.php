<?php
namespace supervillainhq\henchmen\wp\db{
	class WPQuery{
		private $sql;
		private $dbConnector;
		private $parameters;
		private $resultCode;
		private $rows;
		private $wpdb;

		public function __construct($sql){
			$this->sql = $sql;
			$this->parameters = func_get_args(); // this includes the $sql as the first argument in the $this->parameters array, so we don't have to splice the $sql into ot again when executing

			global $wpdb;
			$this->wpdb = $wpdb;
		}

		public function copy(){
			return call_user_func_array(array($this, '__construct'), $this->parameters);
		}

		public function execute(){
			$this->clear();
			if(count($this->parameters)>1){
				# using WP's own anti sql-injection
				$this->sql = call_user_func_array(array($this->wpdb, 'prepare'), $this->parameters);
			}
	// 		call_user_func_array(array($this->wpdb, 'query'), $this->parameters);
			$frags = explode(' ', $this->sql);
			$action = strtolower($frags[0]);
			switch ($action){
				case "select":
					$this->rows = $this->wpdb->get_results($this->sql, ARRAY_A);
					$this->resultCode = count($this->rows);
					break;
				case "insert":
				case "update":
				case "delete":
				default:
					$affectedrows = $this->wpdb->query($this->sql);
					$this->resultCode = $affectedrows;
					if("insert" == $action){
						if(false===$affectedrows){
							$this->resultCode = -1;
							return;
						}
						else if(stripos($this->sql, "on duplicate key")>0 || $this->wpdb->insert_id < 1){
							$this->resultCode = $affectedrows;
						}
						else{
							$this->resultCode = $this->wpdb->insert_id;
						}
					}
					break;
			}
		}

		public function getResultCode(){
			return $this->resultCode;
		}

		public function getValue($key){
			if(!isset($this->rows)){
				return null;
			}
			if(count($this->rows)>0){
				return $this->rows[0][$key];
			}
			return null;
		}

		public function fetchFromResult(array $fields = null){
		}

		public function fetchRows(){
			if(isset($this->rows)){
				return $this->rows;
			}
			return null;
		}

		public function clear(){
			unset($this->rows);
			unset($this->resultCode);
			$this->wpdb->flush();
		}

		public function getInsertId(){
			return $this->wpdb->insert_id;
		}
	}
}
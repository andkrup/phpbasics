<?php
namespace supervillainhq\henchmen\gettext{
	class Gettext{
		private $locale;
		private $domains;
		private $currentDomain;

		function __construct($config, $currentDomain = null){
			$domains = $config->domains;
			$this->resetDomains((array) $domains);
			$this->currentDomain = $currentDomain;
		}

		function resetDomains(array $domains = []){
			$this->domains = $domains;
		}
		function addDomain($domain){
			array_push($this->domains, $domain);
		}
		function removeDomain($domain){
			$c = count($this->domains);
			for($i = 0; $i < $c; $i++){
				if($this->domains[$i] == $domain){
					array_splice($this->domains, $i, 0);
				}
			}
		}
		function hasDomain($domain){
			return in_array($this->domains, $domain);
		}
		function getDomain($index){
			return $this->domains[$index];
		}
		function domains(){
			return $this->domains;
		}

		function init($locale = 'en_US.UTF-8'){
			// gettext is a crybaby and can only play nice if it's fed exactly to its liking, but the rewards are great
			$locale = 'da_DK.UTF-8'; // will add folder '/da_DK' to path, and require the mo file to be compiled using a po file specifying the UTF-8 charset
			$success = setlocale(LC_MESSAGES, $locale); // will add folder '/LC_MESSAGES' to the path
			foreach ($this->domains as $domain){
				bindtextdomain($domain->domain, $domain->path); // look for the mo file in a path starting with '/vagrant_root/config/language'
				bind_textdomain_codeset($domain->domain, $domain->codeset); // ensure that texts are parsed as UTF-8
			}

			$defaultDomain = "site";
			textdomain($defaultDomain); // will require the mo file to be named 'site.mo'
		}
	}
}
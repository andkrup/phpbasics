<?php
/**
 * Created by PhpStorm.
 * User: anders
 * Date: 26/08/16
 * Time: 01:07
 */

namespace supervillainhq\henchmen\jsonrpc{
	interface JsonRpcParser{
		function parse();
	}
}


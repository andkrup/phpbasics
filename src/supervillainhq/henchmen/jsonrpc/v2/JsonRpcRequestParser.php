<?php
/**
 * Created by PhpStorm.
 * User: anders
 * Date: 26/08/16
 * Time: 01:06
 */

namespace supervillainhq\henchmen\jsonrpc\v2 {

	use supervillainhq\henchmen\jsonrpc\JsonRpc;
	use supervillainhq\henchmen\jsonrpc\JsonRpcParser;
	use supervillainhq\henchmen\jsonrpc\JsonRpcRequest;

	class JsonRpcRequestParser implements JsonRpcParser{
		/**
		 * @var
		 */
		private $data;

		function __construct($json){
			$this->data = json_decode($json);
		}

		function parse(){
			if(!property_exists($this->data, 'jsonrpc')){
				return null;
			}
			if(!property_exists($this->data, 'method')){
				return null;
			}
			if(!property_exists($this->data, 'id')){
				return null;
			}
			if(JsonRpc::JSONRPC_V2 != $this->data->jsonrpc){
				throw new \Exception('jsonrpc value must be ' . JsonRpc::JSONRPC_V2);
			}
			$response = new JsonRpcRequest();
			$response->setJsonrpc($this->data->jsonrpc);
			$response->setMethod($this->data->method);
			$response->setId($this->data->id);
			$response->setParams($this->data->params);
			return $response;
		}
	}
}



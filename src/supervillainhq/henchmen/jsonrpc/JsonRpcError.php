<?php
namespace supervillainhq\henchmen\jsonrpc{

	class JsonRpcError extends JsonRpcEnvelope{
		private $code;
		private $message;
		private $data;

		public function __construct(){
			$this->data = null;
		}
		public function getData(){
			return $this->data;
		}
		public function setData($value){
			$this->data = $value;
		}

		public function getMessage(){
			return $this->pay;
		}
		public function setMessage($value){
			$this->message = $value;
		}

		public function getCode(){
			return $this->code;
		}
		public function setCode($value){
			$this->code = $value;
		}

		public function __toString(){
			$data = is_null($this->data) ? 'null' : $this->data;
			return "{\"code\":{$this->code}, \"message\":\"{$this->message}\", \"data\":{$data}}";
		}

		protected function createEnvelope(array $properties = []){
			$properties['message'] = $this->getMessage();
			$properties['code'] = $this->getCode();
			$properties['data'] = $this->getData();
			return parent::createEnvelope($properties);
		}

		function payload($payload = null){
			if(is_null($this->content)){
				return $this->content;
			}
			$this->content = $payload;
		}
	}
}

<?php
namespace supervillainhq\henchmen\jsonrpc {

	use supervillainhq\henchmen\jsonrpc\v1\JsonRpcRequestParser as JsonRpcV1RequestParser;
	use supervillainhq\henchmen\jsonrpc\v2\JsonRpcRequestParser as JsonRpcV2RequestParser;

	final class JsonRpc {
		const JSONRPC_V1 = "1.0";
		const JSONRPC_V2 = "2.0";
		private static $version = "2.0";
		private $queryfactory;
//		private $request;
		private $result;
		private $error;

		private function __construct() {
		}

		static function version(){
			return self::$version;
		}
		static function setVersion($version = "2.0"){
			self::$version = $version;
		}

		static public function parseRequest($input = null) {
			if(is_null($input)){
				$input = file_get_contents("php://input");
			}
			$parser = self::$version == self::JSONRPC_V2 ? new JsonRpcV2RequestParser($input) : new JsonRpcV1RequestParser($input);
			if($request = $parser->parse()){
				return $request;
			}
			throw new \Exception('Could not parse input');
		}

		public function output() {
			$json = new JsonRpcResponse($this->request->getId());
			$json->setJsonrpc('2.0');

			if (isset($this->result) && !isset($this->error)) {
				$json->setResult(json_encode($this->result));
			} else if (isset($this->error)) {
				$error = new JsonError();
				$error->setCode($this->error);
				$error->setMessage($this->fetchErrorMessage($this->error));
				$json->setError($error);
			} else {
				$error = new JsonError();
				$error->setCode(500);
				$error->setMessage('unknown server error');
				$json->setError($error);
			}
			return "$json";
		}

		private function fetchErrorMessage($code) {
			switch ($code) {
				default:
					return "localized error messages not yet implemented";
			}
		}
	}
}
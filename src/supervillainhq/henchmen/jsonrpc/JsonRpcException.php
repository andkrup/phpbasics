<?php
namespace supervillainhq\henchmen\jsonrpc {

	use supervillainhq\core\http\json\Envelope;

	class JsonRpcException extends \Exception implements Envelope{
		private $data;

		public function __construct($message, $data = null, $code = null, $previous = null) {
			parent::__construct($message, $code, $previous);
			$this->data = $data;
		}

		public function getData() {
			return $this->data;
		}

		public function setData($data) {
			$this->data = $data;
		}

		function payload($payload = null){
			// TODO: Implement payload() method.
		}

		/**
		 * Specify data which should be serialized to JSON
		 * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
		 * @return mixed data which can be serialized by <b>json_encode</b>,
		 * which is a value of any type other than a resource.
		 * @since 5.4.0
		 */
		function jsonSerialize(){
			// TODO: Implement jsonSerialize() method.
		}
	}
}
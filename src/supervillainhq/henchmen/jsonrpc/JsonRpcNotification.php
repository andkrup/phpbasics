<?php
namespace supervillainhq\henchmen\jsonrpc {
	class JsonRpcNotification extends JsonRpcEnvelope{
		private $method;
		private $params;

		public function getMethod() {
			return $this->method;
		}

		public function setMethod($value) {
			$this->method = $value;
		}

		public function getParams() {
			return $this->params;
		}

		public function setParams(array $parameters) {
			$this->params = $parameters;
		}


		public function __construct() {
			$this->jsonrpc = "2.0";
		}

		protected function createEnvelope(array $properties = []){
//			$properties['message'] = $this->payload();
			$properties['method'] = $this->getMethod();
			$properties['params'] = $this->getParams();
			return parent::createEnvelope($properties);
		}
	}
}
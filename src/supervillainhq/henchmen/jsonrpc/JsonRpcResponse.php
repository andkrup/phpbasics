<?php
namespace supervillainhq\henchmen\jsonrpc {
	class JsonRpcResponse extends JsonRpcEnvelope{
		private $id;
		private $jsonrpc;
		private $result;
		private $error;

		public function getId() {
			return isset($this->id) ? $this->id : 0;
		}

		public function setId($value) {
			$this->id = $value;
		}

		public function getJsonrpc() {
			return $this->jsonrpc ;
		}

		public function setJsonrpc($value) {
			$this->jsonrpc = $value;
		}

		public function getResult() {
			return $this->payload();
		}

		public function setResult($value) {
			$this->payload($value);
		}

		public function getError() {
			return $this->error;
		}

		public function setError($value) {
			$this->error = $value;
		}

		public function __construct(JsonRpcRequest $request = null, $jsonrpcVersion = '2.0') {
			if (!is_null($request)) {
				$this->setId($request->getId());
			}
			$this->setJsonrpc($jsonrpcVersion);
		}

		protected function createEnvelope(array $properties = []){
			$properties['id'] = $this->getId();
			if($error = $this->getError()){
				$properties['error'] = $error;
			}
			else{
				$properties['result'] = $this->getResult();
			}
			return parent::createEnvelope($properties);
		}

//		public function __toString() {
//			if (isset($this->error)) {
//				return "{\"id\":\"{$this->getId()}\", \"jsonrpc\":\"{$this->getJsonrpc()}\", \"error\":{$this->error}}";
//			} else if (isset($this->result)) {
//				if (is_object($this->result)) {
//					$result = json_encode($this->result);
//				} elseif (is_bool($this->result)) {
//					$result = true == $this->result ? 'true' : 'false';
//				} elseif (is_numeric($this->result)) {
//					$result = intval($this->result);
//				} elseif (is_string($this->result)) {
//					if ($this->result == 'true' || $this->result == 'false') {
//						$result = 'true' == $this->result ? 'true' : 'false';
//					} elseif (strtolower($this->result) == 'null') {
//						$result = 'null';
//					} else {
//						$result = json_encode($this->result);
//					}
//				} elseif (is_null($this->result)) {
//					$result = 'null';
//				} else {
//					$result = json_encode($this->result);
//				}
//				return "{\"id\":\"{$this->getId()}\", \"jsonrpc\":\"{$this->getJsonrpc()}\", \"result\":{$result}}";
//			}
//			return "{\"id\":\"{$this->getId()}\", \"jsonrpc\":\"{$this->getJsonrpc()}\", \"result\":null}";
//		}
	}
}
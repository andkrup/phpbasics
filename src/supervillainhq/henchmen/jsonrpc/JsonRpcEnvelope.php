<?php
/**
 * Created by PhpStorm.
 * User: anders
 * Date: 26/08/16
 * Time: 00:31
 */

namespace supervillainhq\henchmen\jsonrpc {

	use supervillainhq\core\http\json\Envelope;

	class JsonRpcEnvelope implements Envelope{
		protected $jsonrpc; // rpc-spec version
		protected $content;

		public function getJsonrpc() {
			return $this->jsonrpc;
		}

		public function setJsonrpc($value) {
			$this->jsonrpc = $value;
		}

		function payload($payload = null){
			if(is_null($this->content)){
				return $this->content;
			}
			$this->content = $payload;
		}

		/**
		 * Specify data which should be serialized to JSON
		 * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
		 * @return mixed data which can be serialized by <b>json_encode</b>,
		 * which is a value of any type other than a resource.
		 * @since 5.4.0
		 */
		final function jsonSerialize(){
			$properties = [
				'jsonrpc' => $this->jsonrpc,
			];
			$envelope = $this->createEnvelope($properties);
			return $envelope;
		}

		/**
		 * Overload this in order to customise the jsonSerialisable data
		 * @param array $properties
		 * @return array
		 */
		protected function createEnvelope(array $properties = []){
			return $properties;
		}

		function __toString(){
			return json_encode($this);
		}
	}
}



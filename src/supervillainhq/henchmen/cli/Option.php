<?php
/**
 * Created by ak.
 */
namespace supervillainhq\henchmen\cli {

	class Option {
		protected $name;
		protected $argument;

		function __construct($name) {
			$this->name = $name;
		}

		static function input($name, $argument = ''){
			$instance = new Option($name);
			$instance->argument = trim($argument);
		}

		function long(){
			return "--{$this->name}";
		}
		function short(){
			$ch = $this->name{0};
			return "-{$ch}";
		}
		function value($form = null){
			$long = $this->long();
			$short = $this->short();
			// get the value of this argument as if the argument has been input in the long form
			if(is_null($form) || $form == $long){
				$name = $long;
				if(false!==strstr($this->argument, "{$name}=")){
					return trim(str_replace("{$name}=", '', $this->argument));
				}
			}
			// get the value of this argument as if the argument has been input in the abbreviated form
			if(is_null($form) || $form == $short){
				$name = $short;
				if(false!==strstr($this->argument, "{$name}")){
					$frags = preg_split('/[\w:=]/', $this->argument);
					if($frags[0] == $name){
						return trim($frags[1]);
					}
				}
			}
			return null;
		}
		function values($separator = ','){
			$frags = explode($separator, $this->value());
			each($frags, 'trim');
			return $frags;
		}
	}
}

<?php
/**
 * Created by ak.
 */

namespace supervillainhq\henchmen\cli{
	trait Optioning{
		protected $options;

		function resetOptions(array $options = []){
			$this->options = $options;
		}
		function addOption($key, Option $option){
			$this->options[$key] = $option;
		}
		function removeOption(Option $option){
			$c = count($this->options);
			for($i = 0; $i < $c; $i++){
				if($this->options[$i] == $option){
					array_splice($this->options, $i, 0);
				}
			}
		}
		function removeOptionAt($key){
			unset($this->options[$key]);
		}
		function hasOption(Option $option){
			return in_array($this->options, $option);
		}
		function hasOptionAtKey($key){
			return array_key_exists($key, $this->options);
		}
		function getOption($key){
			return $this->options[$key];
		}
		function options(array $options = null){
			if(is_null($options)){
				return $this->options;
			}
			$this->options = $options;
		}
	}
}
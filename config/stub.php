<?php
# stub for the phpunit bootstrap
Phar::mapPhar("phpunit-bootstrap.phar");
require_once "phar://phpunit-bootstrap.phar/ClassLoader.php";
spl_autoload_register(["ClassLoader", "loadClassDefinition"]);
$cwd = getcwd();
$config = include "{$cwd}/config/config.php";
ClassLoader::addSourcePath(realpath($config->application->srcDir));
__HALT_COMPILER();